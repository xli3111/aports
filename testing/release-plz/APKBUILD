# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=release-plz
pkgver=0.3.67
pkgrel=0
pkgdesc="Release Rust packages without using the command line"
url="https://github.com/MarcoIeni/release-plz"
license="MIT OR Apache-2.0"
arch="all"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	openssl-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/MarcoIeni/release-plz/archive/release-plz-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgname-v$pkgver"

prepare() {
	default_prepare
	cargo fetch --locked
}

check() {
	RUST_MIN_STACK=8388608 cargo test --frozen --no-default-features -- \
		--skip "next_ver" \
		--skip "input_generates_correct_release_request"
}

build() {
	OPENSSL_NO_VENDOR=1 cargo auditable build -p "$pkgname" --release --frozen
	mkdir -p completions/
	local compgen="target/release/$pkgname generate-completions"
	$compgen bash >"completions/$pkgname"
	$compgen fish >"completions/$pkgname.fish"
	$compgen zsh >"completions/_$pkgname"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE-MIT -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm 644 "completions/$pkgname" -t "$pkgdir/usr/share/bash-completion/completions/"
	install -Dm 644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d/"
	install -Dm 644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions/"
}

sha512sums="
4a00e732582c45d299dc8de9d3dfebbf6aeb10b2a5135f3803850d80fe20378544517e48b7aabdf816270723d9c3cd51b3af64dfc28172f52f1458afe30a265b  release-plz-v0.3.67.tar.gz
"
