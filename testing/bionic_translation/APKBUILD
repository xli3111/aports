# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bionic_translation
pkgver=0_git20240429
pkgrel=0
_commit="22655af6eed2e7cf4b8d792407c088434eefb028"
pkgdesc="A set of libraries for loading bionic-linked .so files on musl/glibc"
url="https://gitlab.com/android_translation_layer/bionic_translation"
arch="x86_64 aarch64 armv7"
license="Apache-2.0"
makedepends="
	elfutils-dev
	libbsd-dev
	libunwind-dev
	mesa-dev
	meson
	samurai
	"
subpackages="$pkgname-dev $pkgname-dbg"
source="https://gitlab.com/android_translation_layer/bionic_translation/-/archive/$_commit/bionic_translation-$_commit.tar.gz"
builddir="$srcdir/bionic_translation-$_commit"

build() {
	abuild-meson \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
d3244578b1e4e9f9d4d787b5361055ebe0d022b85358100be322c44f744a73969e80341729641d238d03196529f84642458f892a75792c23d4fee88c5eb3752c  bionic_translation-22655af6eed2e7cf4b8d792407c088434eefb028.tar.gz
"
