# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=mitra
pkgver=2.18.0
pkgrel=0
_mitraweb=$pkgver #"${pkgver%.*}.0"
pkgdesc="ActivityPub microblogging platform written in Rust"
url="https://mitra.social/@mitra"
# riscv64: vite webapp fails to build: 'Parse error @:1:38'
arch="all !riscv64"
license="AGPL-3.0-only"
depends="postgresql"
makedepends="
	cargo
	cargo-auditable
	nodejs
	npm
	openssl-dev
	"
install="$pkgname.pre-install $pkgname.post-install"
pkgusers="mitra"
pkggroups="mitra"
subpackages="$pkgname-doc $pkgname-openrc"
source="mitra-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra/archive/v$pkgver.tar.gz
	mitra-web-$_mitraweb.tar.gz::https://codeberg.org/silverpill/mitra-web/archive/v$_mitraweb.tar.gz
	mitra.initd
	init.sql
	config.yaml
	"
builddir="$srcdir/mitra"

# use system openssl
export OPENSSL_NO_VENDOR=1

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked

	cd "$srcdir/mitra-web"

	npm ci --foreground-scripts
}

build() {
	RUSTFLAGS="--cfg=ammonia_unstable" \
	cargo auditable build --frozen --release \
		--features production

	cd "$srcdir/mitra-web"

	echo 'VITE_BACKEND_URL=' > .env.local
	npm run build
}

check() {
	# These tests require a database connection
	RUSTFLAGS="--cfg=ammonia_unstable" \
	cargo test --frozen --workspace \
		--exclude mitra_models -- \
		--skip test_follow_unfollow \
		--skip test_hide_reblogs \
		--skip test_subscribe_unsubscribe \
		--skip test_get_jrd \
		--skip test_filter_mentions_none \
		--skip test_filter_mentions_only_known \
		--skip test_prepare_instance_ed25519_key \
		--skip test_mute

	cd "$srcdir/mitra-web"

	npm run test
}

package() {
	install -Dm755 target/release/mitra -t "$pkgdir"/usr/bin
	install -Dm755 target/release/mitractl -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/webapps
	cp -r "$srcdir"/mitra-web/dist \
		"$pkgdir"/usr/share/webapps/mitra

	install -Dm644 docs/* -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 config_dev.example.yaml \
		contrib/Caddyfile contrib/*.nginx \
		contrib/monero/wallet.conf \
		-t "$pkgdir"/usr/share/doc/$pkgname/examples

	install -Dm640 -g mitra "$srcdir"/config.yaml -t "$pkgdir"/etc/mitra
	install -dm755 -o mitra -g mitra "$pkgdir"/var/lib/mitra
	install -Dm644 "$srcdir"/init.sql -t "$pkgdir"/var/lib/mitra
	install -Dm755 "$srcdir"/mitra.initd "$pkgdir"/etc/init.d/mitra
}

sha512sums="
af75e15dcf30baebc2749822c307350faa0b26aa5deb451c97472d107732dd810da2c2c04332fb8ddda9dc2af67aee23bc1792c9a10e45555cfb02391e8fbaad  mitra-2.18.0.tar.gz
0ad3129576c9e20adf2c61b27d67af1e39446b964fee1fc6f6e2145b3416f1cd4e5d52cc230e0cdce0fab25ff0a9774545cb732d49b010b800d93b29c867b650  mitra-web-2.18.0.tar.gz
691f84f5dfdddc176e75792ab03ff167054246e75ced51be47a89f405ae55ebe5eb6280b73c1b467b5ecbe8539f6108fb3d86873d50fcc4f4b8c5b182632acb0  mitra.initd
180a47f5072534418b4aac3ce7c885a4f7e4dc38aca80d6d81c79848d12fbe24799788c3575bd195030a10da5e0372f87fa2809a4ef99a48eaa6df52f4d053dd  init.sql
ccc0b8efaefdb3ad08838b027895c358595429d511b2ad76502030988485bc86fa53a581bd4afdeedd8967a3d82ada6c30343c826c5dc77e5621e9154b84cee3  config.yaml
"
