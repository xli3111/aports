# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=iperf3
_pkgname=iperf
pkgver=3.17
pkgrel=0
pkgdesc="Tool to measure IP bandwidth using UDP or TCP"
url="https://github.com/esnet/iperf"
arch="all"
license="BSD-3-Clause-LBNL"
makedepends="openssl-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/esnet/$_pkgname/archive/$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd
	"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
6c67dd67076b470db89d7ee5114f3e823e86897aa5fbb1696f7094bd3b39c5a1039e3e7052578e77661006b79f58b0cc33fe18601de4a1e988fc7f0d6b4eba77  iperf3-3.17.tar.gz
fdaf06316886ae02a865848ea6df6b77aecde78fab15bcbc22e077871c3f567521eeee19ef13c402fef467c2edd916a7d976a4c933dbfb637373145a18563ef9  iperf3.initd
4c6b766c154612f5f2e5f6150396f443ba37ec59ed0a8a994bf84612059db22827aee3dd3b7c3249e0bb6037163788d830efcb1caad5eba1c97d2349bdbc55f9  iperf3.confd
"
