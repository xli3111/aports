# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=freetds
pkgver=1.4.13
pkgrel=0
pkgdesc="Tabular Datastream Library"
url="https://www.freetds.org/"
arch="all"
license="GPL-2.0-or-later OR LGPL-2.0-or-later"
makedepends="openssl-dev>3 linux-headers readline-dev unixodbc-dev"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://www.freetds.org/files/stable/freetds-$pkgver.tar.bz2"
options="!check"  # tests require running SQL server http://www.freetds.org/userguide/confirminstall.htm#TESTS

prepare() {
 	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-msdblib \
		--with-openssl=/usr \
		--enable-odbc \
		--with-unixodbc=/usr
	make
}

check() {
	cd "$builddir"/src/replacements
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
4e25127bc8f324c1d2c00adafa02cad8214172b85b15aad44f01b6dd51034d3d245a89f0e085d8518ec8c7d889e3af3b56eb8cb49419e530b92c023a8df55d7d  freetds-1.4.13.tar.bz2
"
