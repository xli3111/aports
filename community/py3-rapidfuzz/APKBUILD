# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-rapidfuzz
pkgver=3.9.0
pkgrel=0
pkgdesc="Rapid fuzzy string matching in Python using various string metrics"
url="https://github.com/maxbachmann/RapidFuzz"
arch="all"
license="MIT"
makedepends="
	cmake
	cython
	py3-gpep517
	py3-rapidfuzz-capi
	py3-scikit-build
	py3-setuptools
	python3-dev
	samurai
	"
checkdepends="
	py3-hypothesis
	py3-numpy
	pytest
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/r/rapidfuzz/rapidfuzz-$pkgver.tar.gz"
builddir="$srcdir/rapidfuzz-$pkgver"

case "$CARCH" in
x86*)
	# float rounding
	options="$options !check"
	;;
esac

build() {
	RAPIDFUZZ_BUILD_EXTENSION=1 \
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2 -U_FORTIFY_SOURCE" \
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer \
		.dist/rapidfuzz*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/rapidfuzz*.whl
}

sha512sums="
bfd0d399dc75c6654d5e66522a057298d3914f8b74955e873034b314926127971f1bf4ddca0c2c92e81ba5bae79caf249303b169805d48ef3d6c64fb706c9547  rapidfuzz-3.9.0.tar.gz
"
