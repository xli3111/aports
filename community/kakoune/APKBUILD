# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Willow Barraco <contact@willowbarraco.fr>
pkgname=kakoune
pkgver=2024.05.09
pkgrel=1
pkgdesc="Code editor heavily inspired by Vim, but with less keystrokes"
url="https://kakoune.org"
arch="all"
license="Unlicense"
checkdepends="perl"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/mawww/kakoune/archive/v$pkgver.tar.gz
	alpine-linux.kak
	0001-fix-CXXFLAGS.patch
	constexpr.patch
	fix-tests-1.patch
	fix-tests-2.patch
	"

build() {
	make debug=no
}

check() {
	make check
}

package() {
	make -j1 PREFIX="/usr" DESTDIR="$pkgdir/" debug=no install
	install -Dm644 "$srcdir"/alpine-linux.kak \
		-t "$pkgdir"/usr/share/kak/autoload/filetype/
}

sha512sums="
e510b01574d505aa99e1927b2ef43c52ef866163fcc59a334951159c63f21ce671e475aba7fcf79a284105ff8bebf76f8e987799cb415e1503dbe1d1dd4fe2c4  kakoune-2024.05.09.tar.gz
c5b11dc28adf785b4e83637fe1d5c9db334616d2d3f6c667823745e273f739c57d0d408b9bec8a65210e3c7ccb80a5b488ebbb3c0c5de8bdec13e1b4b4d02b77  alpine-linux.kak
83295606d1191ae66339ccd813b5f123ab49b22bafcdd578b109ea367dae52c5de7f6b8e44119579163befb349e3b76b4b9e4b20f951466dc2c270c38e64ceb1  0001-fix-CXXFLAGS.patch
aa5df7a115e7b7ecaba85794ea56961fd1c638f4590300474fa72c8900ecb36742d7c8075ebc48151f40ba2dc427c4d8bcca9c844793768a1d0c4e66f22d328c  constexpr.patch
3ad545cbbc23f0273032543a24e380236e149dfed540da7b51c073b3c07107d3c7a4f0ff17c739fc45c41e0d52e2b6608979f5e1d1d1f60e774994a330027fa1  fix-tests-1.patch
a5000d3605e45a0d26f05a4d79e7c0993f4c45c3a6ccd07817a7ba73c10a7af0c30fa36d2cb013cb96252684b602f71d1c87085ba1df91072f9a5ba29b6bcbfd  fix-tests-2.patch
"
