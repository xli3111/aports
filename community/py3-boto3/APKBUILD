# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-boto3
pkgver=1.34.104
pkgrel=0
pkgdesc="AWS SDK for Python (Boto3)"
url="https://aws.amazon.com/sdk-for-python/"
license="Apache-2.0"
arch="noarch"
depends="
	py3-botocore
	py3-jmespath
	py3-s3transfer
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/boto3/boto3-$pkgver.tar.gz"
builddir="$srcdir"/boto3-$pkgver
options="!check" # take way too long

replaces="py-boto3" # Backwards compatibility
provides="py-boto3=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
2d155972354e47f59f172191003815c30ecce32478871ae72547bbfccf300842ccb46ee3d2da5ce81f59482ec9e3d8848f741a729447a47868a378f72cb77066  boto3-1.34.104.tar.gz
"
